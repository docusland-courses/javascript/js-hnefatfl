# Javascript hnefatfl

> Le hnefatfl est un jeu d'origine scandinave connu dès le Ve siècle de notre ère.
> Les vikings l'exportent en Irlande et en Angleterre. 

> Le hnefatfl aura des nommages différents au fil des années (Tablut ou encore Alea Evangelii) ainsi que quelques variations de règles avant d'être détrôné par les échecs. 

## Règles du jeu de hnefatfl

Il existe plusieurs variantes, nous allons en implémenter une seule. 
Il vous est possible de tester ce jeu sur ce site [http://hnefatafl.fr/](http://hnefatafl.fr/).

**Matériel :**
  - Un plateau de jeu (9x9)
  - 9 pions d'une couleur pour les défenseurs (dont un roi)
  - 16 pions d'une autre couleur pour les assaillants

**But du jeu :**
Ce jeu se joue à deux joueurs. 
Deux armées s'affrontent : 
  - Une armée de défenseurs, composée d'un roi et de 8 soldats.
  - Une armée d'assaillants

Le joueur en position de défenseur gagne la partie s'il détruit l'armée des assaillants ou si le roi parvient à s'échapper en atteignant un bord du plateau.

Le joueur assaillant gagne la partie s'il capture le roi.

**Mise en place :**
Les pions sont disposés sur le plateau. Un joueur incarne les assaillants (pions rouge), l'autre les défenseurs (pions bleu). Le roi (pions cyan) se trouve sur la case centrale nommée __konakis__ (le trône).

<style>
  tr, td {
    border: 1px solid black;
  }
  td { width: 16px; height: 16px;}
  .assaillant {
    background-color: red;
  }

  .defenseur{
    background-color: blue;
    color: white;
  }
  .konakis {
    border: 3px solid gold;
  }
  .roi {
    background-color: cyan;
  }
</style>

<center>
<table>
<tr>
  <td></td>
  <td></td>
  <td></td>
  <td class='assaillant'>A</td>
  <td class='assaillant'>A</td>
  <td class='assaillant'>A</td>
  <td></td>
  <td></td>
  <td></td>
</tr>
<tr>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td class='assaillant'>A</td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
</tr>
<tr>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td class='defenseur'>D</td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
</tr>
<tr>
  <td class='assaillant'>A</td>
  <td></td>
  <td></td>
  <td></td>
  <td class='defenseur'>D</td>
  <td></td>
  <td></td>
  <td></td>
  <td class='assaillant'>A</td>
</tr>
<tr>
  <td class='assaillant'>A</td>
  <td class='assaillant'>A</td>
  <td class='defenseur'>D</td>
  <td class='defenseur'>D</td>
  <td class='konakis roi'>R</td>
  <td class='defenseur'>D</td>
  <td class='defenseur'>D</td>
  <td class='assaillant'>A</td>
  <td class='assaillant'>A</td>
</tr>
<tr>
  <td class='assaillant'>A</td>
  <td></td>
  <td></td>
  <td></td>
  <td class='defenseur'>D</td>
  <td></td>
  <td></td>
  <td></td>
  <td class='assaillant'>A</td>
</tr>
<tr>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td class='defenseur'>D</td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
</tr>
<tr>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <td class='assaillant'>A</td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
</tr>
<tr>
  <td></td>
  <td></td>
  <td></td>
  <td class='assaillant'>A</td>
  <td class='assaillant'>A</td>
  <td class='assaillant'>A</td>
  <td></td>
  <td></td>
  <td></td>
</tr>
</table>
</center>

**Règles :**
- **Déplacement des pions :** Les joueurs jouent à tour de rôle. Tous les pions se déplacent de la même manière, d'un nombre illimité de cases, à la verticale ou à l'horizontale et ne peuvent "sauter" un pion.
- **Capture des pions :** Si un joueur enferme un pion adverse entre deux de ses pions (sur une verticale ou sur une horizontale, jamais en diagonale), il le capture et le retire du jeu.
Toutefois, si le joueur place volontairement un pion entre deux pions adverses, le pion ne peut être pris.
- **Déplacement du roi :** Lorsqu'une voie est ouverte vers l'un des bords du plateau, le roi dit "raichi". Si deux voies sont ouvertes, le roi dit "bushi" et la partie est gagnée d'office puisque le joueur adverse ne peut bloquer deux passages sur un seul coup.
- **Capture du roi :** Pour prendre le roi, l'assaillant doit l'enfermer entre quatre de ses pions, trois si le dernier côté est occupée par le __konakis__.

## Consignes
Vous avez pour objectif de réaliser ce petit jeu individuellement. 

Le jeu devra être : 
 + versionné sur une plateforme git
 + responsive (Adapté pour téléphone, tablette et PC)
 + accessible (Attention aux dimensions et aux contrastes)
 + intuitif : Des règles doivent être disponibles sur le site 

Pour information, les couleurs spécifiées dans ce README sont optionnelles. Il s'agissait juste de clarifier les règles. Stylisez comme bon vous semble votre jeu ! 

Privilégiez la qualité à la quantité! Avancez méthodiquement. 
 
## Méthode
 - Définir une charte graphique
 - Définir les pages et de l'interface utilisateur
 - Définir  les maquettes
 - Avancer par le biais d'issues git afin de structurer l'approche
 - Scinder proprement le code
 - Documenter tout au sein d'un fichier `README.md`
